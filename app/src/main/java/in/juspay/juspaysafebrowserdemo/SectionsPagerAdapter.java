package in.juspay.juspaysafebrowserdemo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages. This contains Webview and Juspay FrameLayout
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    List<PlaceholderFragment> listFragments = new ArrayList<PlaceholderFragment>();
    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        if(listFragments.size()<=position){
            listFragments.add(PlaceholderFragment.newInstance(position + 1));
            return listFragments.get(position);
        }else{
            return listFragments.get(position);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Tab 1";
            case 1:
                return "Tab 2";
            case 2:
                return "Tab 3";
        }
        return null;
    }
}