package in.juspay.juspaysafebrowserdemo;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.v2.JuspayBrowserFragmentFactory;

public class CustomWebviewClient extends WebViewClient {
    private JuspayBrowserFragment browserFragment = null;
    private WebView webview;
    private FrameLayout frameLayout;
    private android.support.v4.app.FragmentManager childFragmentManager;

    CustomWebviewClient(WebView webview, FrameLayout frameLayout,android.support.v4.app.FragmentManager fragmentManager){
        this.webview = webview;
        this.frameLayout = frameLayout;
        this.childFragmentManager = fragmentManager;
        setupKeyboardUpEvent(webview);
    }
    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed();
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        return false;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if (browserFragment != null) {
            browserFragment.onPageFinished(view, url);
        }

        //Destroying Instance when payment response URL is loaded
        if (url.contains("https://www.billdesk.com/pgidsk/wap/vodafonetup/vodafonewapResponse.jsp")) {
            JuspayBrowserFragmentFactory.destroyInstance(browserFragment, childFragmentManager);
        }
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        // Instantiating JuspayBrowserFragment when Payment URL is found loading
        if (url.contains("https://www.billdesk.com/pgidsk/wap/vodafonetup/vodafonechordiantconfirm.jsp")) {
            Bundle argumentsBundle = new Bundle();
            argumentsBundle.putString("merchantId", "uc_browser");
            argumentsBundle.putString("clientId", "uc_browser_android");
            browserFragment = JuspayBrowserFragmentFactory.newInstance(webview, frameLayout, childFragmentManager, argumentsBundle);
        }
        if (browserFragment != null) {
            browserFragment.onPageStarted(view, url, favicon);
        }
    }

    /**
     * This is merchants own logic to find out the event of keyboard coming up.
     * Logic here is just for demo and may not be true in all cases.
     * @param rootView
     */
    private void setupKeyboardUpEvent(final View rootView){
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                rootView.getWindowVisibleDisplayFrame(r);
                int screenHeight = rootView.getRootView().getHeight();

                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    if(browserFragment!=null){
                        browserFragment.onKeyboardUp();
                    }
                } else {
                    // keyboard is closed
                }
            }
        });
    }
}
