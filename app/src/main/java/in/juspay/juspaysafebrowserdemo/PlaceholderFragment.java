package in.juspay.juspaysafebrowserdemo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.FrameLayout;

/**
 * A placeholder fragment containing a simple webview and FrameLayout for JuspayFragment to come in.
 */
public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public PlaceholderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        String URL = "https://www.billdesk.com/pgidsk/wap/vodafonetup/vodafonechordiantconfirm.jsp";
        String POSTDATA = "Pre_bonuscards1$hdnAccessfee=1.00&" +
                "Pre_bonuscards1$hdnBenefit=Talktime of Rs.7.9&" +
                "Pre_bonuscards1$hdnChannelId=RECHARGE&" +
                "Pre_bonuscards1$hdnCircleId=&" +
                "Pre_bonuscards1$hdnMRP=10.00&" +
                "Pre_bonuscards1$hdnMobileNumber=9620917775&" +
                "Pre_bonuscards1$hdnServiceTax=12.36&" +
                "Pre_bonuscards1$hdnTalktime=7.90&" +
                "Pre_bonuscards1$hdnaction=confirm_etopup&" +
                "Pre_bonuscards1$hdnairtime=0.00&" +
                "Pre_bonuscards1$hdnmsg=9620917775|10.00|0.00|1.00|12.36|Talktime of Rs.7.9|kar|RECHARGE|7.90|20140505170308|2144186221&" +
                "Pre_bonuscards1$txtDenom=10&" +
                "Pre_bonuscards1$txtmobileNo=9620917775&" +
                "__EVENTARGUMENT=&" +
                "__EVENTTARGET=&" +
                "__EVENTVALIDATION=/wEWCwLtyvX4DgLB39rQDgLIkufkCALl1YjvDALGn/GnAwKc6s+nCALI1fSCCQLI1bjxDgLI1czMBwLJgK/wCgKuwITEBoKlRHFTXgI5XV82o1qSAOneVyq1";

        args.putString("URL",URL);
        args.putString("POSTDATA",POSTDATA);
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_browser_tabbed, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String URL = getArguments().getString("URL");
        String POSTDATA = getArguments().getString("POSTDATA");
        final WebView webview = (WebView)view.findViewById(R.id.browser_Webview);
        final FrameLayout frameLayout = (FrameLayout)view.findViewById(R.id.juspay_fragment_to_be_replaced);
        webview.getSettings().setDomStorageEnabled(true);
        webview.setWebViewClient(new CustomWebviewClient(webview,frameLayout,getChildFragmentManager()));
        webview.postUrl(URL, POSTDATA.getBytes());
    }
}