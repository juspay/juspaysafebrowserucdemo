# Juspay Safe Browser Integration Document for Android
JusPay Safe Browser (codename: Godel) aims to reduce friction in Second Factor Authentication for Cards and Netbanking.

The library provides various convenience and security features for users to be able to complete the transaction quickly. The library also provides much deeper insight into the events occurring in the payment flow. With Godel, you will be able to provide a pleasing payments experience to your Android users.

This documentation explains the steps to integrate the library into your native Android application.

### Get Godel SDK 
The library can be added as a direct dependency when you use Gradle. If you don’t use Gradle, we provide a downloadable SDK which you can add as a project dependency.

Add the following maven repository to the build.gradle:
```
repositories {
  mavenCentral()
  maven {
    url "https://s3-ap-southeast-1.amazonaws.com/godel-release/godel/"
  }
}

dependencies {
  compile 'com.android.support:support-v4:+'
  compile 'in.juspay:godel:0.6.9_uc'
}
```

### Setup Permissions

JusPay Safe browser currently supports all applications using version >= Android API version 8.
The following are the minimum set of permissions required for the library to work as expected:

```
<uses-permission android:name="android.permission.READ_SMS" />
<uses-permission android:name="android.permission.RECEIVE_SMS" />
<uses-permission android:name="android.permission.READ_PHONE_STATE"/>
```

### FrameLayout in the existing Layout
Juspay's sdk require a FrameLayout where sdk can add or remove fragments based on the page that is loaded in webview. This shall be a sibling of Webview and must have LinearLayout with vertical orientation as a parent. Code snippet can be as follows:
```
<LinearLayout android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <WebView
        android:id="@+id/browser_Webview"
        android:layout_width="match_parent"
        android:layout_weight="1"
        android:layout_height="0dp" />
    <FrameLayout
        android:layout_gravity="bottom"
        android:id="@+id/juspay_fragment_to_be_replaced"
        android:layout_width="match_parent"
        android:layout_height="wrap_content" />

</LinearLayout>
```

### Instantiate Juspay Browser Fragment
This can be done via Factory method available and as an arguments to the constructor, it expects following:
 - Webview : Webview on which actions has to be performed
 - FrameLayout: This is a layout where Juspay adds fragments for users
 - FragmentManager: This is a ChildFragmentManager using which Juspay can add and replace fragments to and from the FrameLayout passed
 - Bundle: This contains other arguments that can be passed for the transaction and merchant details like clientId, merchantId, etc.

Juspay Fragment can be initiated whenever merchant finds a URL that has started loading is payment URL and this can be done as follows:
```
JuspayBrowserFragment browserFragment = JuspayBrowserFragmentFactory.newInstance(webview, frameLayout, getChildFragmentManager(),argumentsBundle);
```
This can be considered as the initialization point for Juspay's SDK. 

### Passing Webview Events to JuspayBrowserFragment Instance
Merchants are required to pass onPageStarted and onPageFinished events to the instance so that instance can now act upon it to show associated fragment. This can be done as follows:  

```
@Override
public void onPageFinished(WebView view, String url) {
    browserFragment.onPageFinished(view, url);
}

@Override
public void onPageStarted(WebView view, String url, Bitmap favicon) {
    browserFragment.onPageStarted(view, url, favicon);
}
```

### Passing Keyboard Up Event
Merchant can write their own logic to determine keyboard up event in their app. So whenever this event is triggered for merchant app, pass it to JuspayBrowserFragment instance as follows:
```
browserFragment.onKeyboardUp();
```

### Destroying JuspayBrowserFragment Instance
Whenever payment url is loaded where merchant wants to destroy the fragment object, please pass the object to factory method along with childFragmentManager as follows:
```
JuspayBrowserFragmentFactory.destroyInstance(browserFragment,getChildFragmentManager());
```